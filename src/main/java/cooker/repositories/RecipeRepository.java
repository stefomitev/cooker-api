package cooker.repositories;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import cooker.models.Recipe;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long> {
    @Query(value = "SELECT r FROM Recipe r WHERE r.authorId = :authorId")
    Page<Recipe> getRecipesForAuthor(@Param("authorId") Long authorId, Pageable pageable);

    @Query(value = "SELECT r FROM Recipe r WHERE r.authorId in :authorIds")
    List<Recipe> getRecipesForAuthors(@Param("authorIds") List<Long> authorIds);
}
