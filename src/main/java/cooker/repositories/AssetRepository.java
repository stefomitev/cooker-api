package cooker.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import cooker.models.Asset;

public interface AssetRepository extends PagingAndSortingRepository<Asset, Long> {
    @Query(value = "SELECT a FROM Asset a WHERE a.recipeId in :recipesIds", name = "AssetRepository.assetsForRecipes")
    List<Asset> getAssetsForRecipes(@Param("recipesIds") List<Long> recipesIds);
}
