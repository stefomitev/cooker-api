package cooker.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import cooker.models.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	Optional<User> findByUsername(String username);

	@Query(value = "SELECT u FROM User u, Recipe r WHERE r.authorId = u.id AND r.id in :recipeIds")
	List<User> getAuthorsForRecipes(@Param("recipeIds") List<Long> recipeIds);
}
