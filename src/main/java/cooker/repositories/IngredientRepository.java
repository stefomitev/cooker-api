package cooker.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import cooker.models.Ingredient;
import cooker.models.RecipeIngredient;

public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {
    @Query(value = "SELECT rI FROM RecipeIngredient rI WHERE rI.recipeId in :recipeIds")
    List<RecipeIngredient> getIngredientsForRecipes(@Param("recipeIds") List<Long> recipeIds);
}
