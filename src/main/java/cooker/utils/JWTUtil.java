package cooker.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.crypto.SecretKey;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@Component
public class JWTUtil {
	private SecretKey signKey;

	@Value("${security.token.sign_secret}")
	private String tokenSignSecret;

	@Value("${security.token.expiry}")
	private Integer tokenExpiry;
	
	public String getUsernameFromToken(String token) {
		return getAllClaimsFromToken(token).getSubject();
	}

	public Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(signKey).parseClaimsJws(token).getBody();
	}
	
	public String generateToken(UserDetails user) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(user.getUsername(), claims);
	}

	private String doGenerateToken(String username, Map<String, Object> claims) {
		final Date createdDate = new Date();
		final Date expirationDate = new Date(createdDate.getTime() + tokenExpiry);
		
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(username)
				.setIssuedAt(createdDate)
				.setExpiration(expirationDate)
				.signWith(signKey)
				.compact();
	}
	
	@PostConstruct
	private void postConstruct() {
		signKey = Keys.hmacShaKeyFor(tokenSignSecret.getBytes());
	}
}
