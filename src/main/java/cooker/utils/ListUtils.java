package cooker.utils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * ListUtils
 */
public class ListUtils {
    public static <K, V> List<List<V>> mapForKeys(List<K> keys, List<V> values, Function<V, K> keyProvider) {
        Map<K, List<V>> map = keys.parallelStream().collect(Collectors.<K, K, List<V>>toMap(key -> key, key -> values
                .parallelStream().filter(value -> key.equals(keyProvider.apply(value))).collect(Collectors.toList())));
        return map.values().parallelStream().collect(Collectors.toList());
    }
}
