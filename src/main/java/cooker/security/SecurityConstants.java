package cooker.security;

public final class SecurityConstants {
	public static final String AUTH_HEADER = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer ";
}
