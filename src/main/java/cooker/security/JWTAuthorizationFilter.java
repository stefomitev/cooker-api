package cooker.security;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import cooker.models.User;
import cooker.utils.JWTUtil;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	private final JWTUtil jwtUtil;
	private final UserDetailsService userDetailsService;

	public JWTAuthorizationFilter(JWTUtil jwtUtil, UserDetailsService userDetailsService, AuthenticationManager authenticationManager) {
		super(authenticationManager);
		this.jwtUtil = jwtUtil;
		this.userDetailsService = userDetailsService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String username;
		Optional<String> headerOpt = Optional.ofNullable(request.getHeader(SecurityConstants.AUTH_HEADER));
		
		if (headerOpt.isPresent()) {
			String token = headerOpt.get().replaceAll(SecurityConstants.TOKEN_PREFIX, "");
			username = jwtUtil.getUsernameFromToken(token);
		} else {
			username = User.ANONYMOUS_NAME;
		}
		
		UserDetails user = userDetailsService.loadUserByUsername(username);
		Authentication auth = new UsernamePasswordAuthenticationToken(user.getUsername(), "", user.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);

		chain.doFilter(request, response);
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}
}
