package cooker.graphql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;
import javax.websocket.server.HandshakeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import cooker.graphql.loaders.DataLoaderRegistryBuilder;
import graphql.servlet.context.DefaultGraphQLContext;
import graphql.servlet.context.DefaultGraphQLServletContext;
import graphql.servlet.context.DefaultGraphQLWebSocketContext;
import graphql.servlet.context.GraphQLContext;
import graphql.servlet.context.GraphQLContextBuilder;

@Component
public class ContextBuilder implements GraphQLContextBuilder {
    @Autowired
    private DataLoaderRegistryBuilder dataLoaderRegistryBuilder;

    @Override
    public GraphQLContext build(HttpServletRequest request, HttpServletResponse response) {
        return DefaultGraphQLServletContext.createServletContext(dataLoaderRegistryBuilder.build(), null).with(request)
                .with(response).build();
    }

    @Override
    public GraphQLContext build(Session session, HandshakeRequest request) {
        return DefaultGraphQLWebSocketContext.createWebSocketContext(dataLoaderRegistryBuilder.build(), null)
                .with(session).with(request).build();
    }

    @Override
    public GraphQLContext build() {
        return new DefaultGraphQLContext(dataLoaderRegistryBuilder.build(), null);
    }
}
