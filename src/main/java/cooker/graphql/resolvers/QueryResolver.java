package cooker.graphql.resolvers;

import java.util.Optional;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import cooker.graphql.models.GqlAsset;
import cooker.graphql.models.GqlIngredient;
import cooker.graphql.models.GqlRecipe;
import cooker.graphql.models.GqlUser;
import cooker.repositories.AssetRepository;
import cooker.repositories.IngredientRepository;
import cooker.repositories.RecipeRepository;
import cooker.repositories.UserRepository;

@Component
public class QueryResolver implements GraphQLQueryResolver {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RecipeRepository recipeRepo;

    @Autowired
    private AssetRepository assetRepo;

    @Autowired
    IngredientRepository ingredientRepo;

    public GqlUser getUser(Long id) {
        return new GqlUser(userRepo.findById(id).get());
    }

    public GqlRecipe getRecipe(Long id) {
        return new GqlRecipe(recipeRepo.findById(id).get());
    }

    public GqlAsset getAsset(Long id) {
        return new GqlAsset(assetRepo.findById(id).get());
    }

    public Iterable<GqlUser> getUsers(Optional<Integer> page, Optional<Integer> pageSize) {
        return userRepo.findAll(PageRequest.of(page.orElse(0), pageSize.orElse(12))).map(user -> new GqlUser(user));
    }

    public Iterable<GqlRecipe> getRecipes(Optional<Integer> page, Optional<Integer> pageSize, Optional<Long> authorId) {
        if (authorId.isPresent()) {
            return recipeRepo.getRecipesForAuthor(authorId.get(), PageRequest.of(page.orElse(0), pageSize.orElse(12)))
                    .map(recipe -> new GqlRecipe(recipe));
        } else {
            return recipeRepo.findAll(PageRequest.of(page.orElse(0), pageSize.orElse(12)))
                    .map(recipe -> new GqlRecipe(recipe));
        }
    }

    public Iterable<GqlAsset> getAssets(Optional<Integer> page, Optional<Integer> pageSize) {
        return assetRepo.findAll(PageRequest.of(page.orElse(0), pageSize.orElse(12))).map(asset -> new GqlAsset(asset));
    }

    public Iterable<GqlIngredient> getIngredients(Optional<Integer> page, Optional<Integer> pageSize) {
        return ingredientRepo.findAll(PageRequest.of(page.orElse(0), pageSize.orElse(12)))
                .map(ingredient -> new GqlIngredient(ingredient));
    }

    public GqlUser getMe() {
        Optional<String> usernameOpt = Optional
                .ofNullable((String) SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        return usernameOpt
                .map(username -> userRepo.findByUsername(username).map(user -> new GqlUser(user)).orElse(null))
                .orElseThrow(() -> new RuntimeException("current user not found"));
    }
}
