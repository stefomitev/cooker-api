package cooker.graphql.resolvers;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;
import cooker.graphql.loaders.DataLoaderRegistryBuilder;
import cooker.graphql.models.GqlRecipe;
import cooker.graphql.models.GqlUser;
import graphql.schema.DataFetchingEnvironment;

@Component
public class UserResolver implements GraphQLResolver<GqlUser> {
    public CompletableFuture<List<GqlRecipe>> getRecipes(GqlUser user, DataFetchingEnvironment env) {
        DataLoader<Long, List<GqlRecipe>> loader = env.getDataLoader(DataLoaderRegistryBuilder.RECIPES_FOR_AUTHORS);
        return loader.load(user.id);
    }
}
