package cooker.graphql.resolvers;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;
import cooker.graphql.loaders.DataLoaderRegistryBuilder;
import cooker.graphql.models.GqlAsset;
import cooker.graphql.models.GqlRecipe;
import cooker.graphql.models.GqlRecipeIngredient;
import cooker.graphql.models.GqlUser;
import graphql.schema.DataFetchingEnvironment;

@Component
public class RecipeResolver implements GraphQLResolver<GqlRecipe> {
    public CompletableFuture<GqlUser> getAuthor(GqlRecipe recipe, DataFetchingEnvironment env) {
        DataLoader<Long, GqlUser> loader = env.getDataLoader(DataLoaderRegistryBuilder.AUTHORS_FOR_RECIPES);
        return loader.load(recipe.id);
    }

    public CompletableFuture<List<GqlAsset>> getAssets(GqlRecipe recipe, DataFetchingEnvironment env) {
        DataLoader<Long, List<GqlAsset>> loader = env.getDataLoader(DataLoaderRegistryBuilder.ASSETS_FOR_RECIPES);
        return loader.load(recipe.id);
    }

    public CompletableFuture<List<GqlRecipeIngredient>> getIngredients(GqlRecipe recipe, DataFetchingEnvironment env) {
        DataLoader<Long, List<GqlRecipeIngredient>> loader = env
                .getDataLoader(DataLoaderRegistryBuilder.INGREDIENTS_FOR_RECIPES);
        return loader.load(recipe.id);
    }
}
