package cooker.graphql.resolvers;

import java.util.Optional;
import java.util.stream.Collectors;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import cooker.graphql.models.GqlIngredient;
import cooker.graphql.models.GqlRecipe;
import cooker.graphql.models.GqlUser;
import cooker.graphql.models.IngredientInput;
import cooker.graphql.models.RecipeInput;
import cooker.graphql.models.UserInput;
import cooker.models.Ingredient;
import cooker.models.Recipe;
import cooker.models.RecipeIngredient;
import cooker.repositories.IngredientRepository;
import cooker.repositories.RecipeRepository;
import cooker.repositories.UserRepository;

@Component
public class MutationResolver implements GraphQLMutationResolver {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RecipeRepository recipeRepo;

    @Autowired
    private IngredientRepository ingredientRepo;

    @Autowired
    private BCryptPasswordEncoder passEncoder;

    public GqlRecipe createRecipe(RecipeInput input) {
        Recipe recipe = input.toModel();
        recipe.setAuthorId(input.getAuthorId());

        recipe.setIngredients(input.getIngredients().parallelStream().map(i -> {
            RecipeIngredient recipeIngredient = new RecipeIngredient();
            Optional<Ingredient> ingOpt = ingredientRepo.findById(i.getIngredientId());

            if (ingOpt.isPresent()) {
                recipeIngredient.setIngredientId(ingOpt.get().getId());
                recipeIngredient.setQuantity(i.getQuantity());
                return recipeIngredient;
            }

            return null;
        }).filter(ing -> ing != null).collect(Collectors.toList()));

        return new GqlRecipe(recipeRepo.save(recipe));
    }

    public GqlUser createUser(UserInput input) {
        input.setPassword(passEncoder.encode(input.getPassword()));
        return new GqlUser(userRepo.save(input.toModel()));
    }

    public GqlIngredient createIngredient(IngredientInput input) {
        return new GqlIngredient(ingredientRepo.save(input.toModel()));
    }
}
