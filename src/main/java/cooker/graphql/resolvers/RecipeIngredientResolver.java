package cooker.graphql.resolvers;

import java.util.concurrent.CompletableFuture;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;
import cooker.graphql.loaders.DataLoaderRegistryBuilder;
import cooker.graphql.models.GqlIngredient;
import cooker.graphql.models.GqlRecipeIngredient;
import graphql.schema.DataFetchingEnvironment;

@Component
public class RecipeIngredientResolver implements GraphQLResolver<GqlRecipeIngredient> {
    public CompletableFuture<GqlIngredient> getIngredient(GqlRecipeIngredient recipeIngredient,
            DataFetchingEnvironment env) {
        DataLoader<Long, GqlIngredient> loader = env.getDataLoader(DataLoaderRegistryBuilder.INGREDIENTS);
        return loader.load(recipeIngredient.ingredientId);
    }
}
