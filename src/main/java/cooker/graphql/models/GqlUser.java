package cooker.graphql.models;

import cooker.models.User;

public class GqlUser extends GqlBase<User> {
    public String email;
    public String username;
    public String shortBio;

    public GqlUser(User instance) {
        super(instance);
        this.email = instance.getEmail();
        this.username = instance.getUsername();
        this.shortBio = instance.getShortBio();
    }
}
