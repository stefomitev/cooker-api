package cooker.graphql.models;

import cooker.models.RecipeIngredient;

public class GqlRecipeIngredient extends GqlBase<RecipeIngredient> {
    public Long ingredientId;
    public Long recipeId;
    public Long quantity;

    public GqlRecipeIngredient(RecipeIngredient instance) {
        super(instance);
        this.ingredientId = instance.getIngredientId();
        this.recipeId = instance.getRecipeId();
        this.quantity = instance.getQuantity();
    }
}
