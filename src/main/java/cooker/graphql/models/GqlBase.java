package cooker.graphql.models;

import java.util.Date;
import cooker.models.Base;

public abstract class GqlBase<T extends Base> {
    public Long id;
    public Date createdOn;
    public Date modifiedOn;

    public GqlBase(T instance) {
        this.id = instance.getId();
        this.createdOn = instance.getCreatedOn();
        this.modifiedOn = instance.getModifiedOn();
    }
}
