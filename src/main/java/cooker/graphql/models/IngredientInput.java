package cooker.graphql.models;

import cooker.models.Ingredient;

public class IngredientInput {
    private String name;
    private String baseUnit;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUnit() {
        return this.baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Ingredient toModel() {
        Ingredient ingredient = new Ingredient();
        ingredient.setName(name);
        ingredient.setBaseUnit(baseUnit);
        return ingredient;
    }
}
