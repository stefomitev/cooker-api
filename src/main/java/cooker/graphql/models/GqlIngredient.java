package cooker.graphql.models;

import cooker.models.Ingredient;

public class GqlIngredient extends GqlBase<Ingredient> {
    public String name;
    public String baseUnit;

    public GqlIngredient(Ingredient instance) {
        super(instance);
        this.name = instance.getName();
        this.baseUnit = instance.getBaseUnit();
    }
}
