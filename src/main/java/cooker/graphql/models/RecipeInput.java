package cooker.graphql.models;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import cooker.models.Recipe;

public class RecipeInput {
    private String title;
    private String excerpt;
    private Integer servings;
    private Integer prepTime;
    private String nutritionFacts;
    private String prepSteps;
    private Long authorId;
    private Collection<RecipeIngredientInput> ingredients;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return this.excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public Integer getServings() {
        return this.servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public Integer getPrepTime() {
        return this.prepTime;
    }

    public void setPrepTime(Integer prepTime) {
        this.prepTime = prepTime;
    }

    public String getNutritionFacts() {
        return this.nutritionFacts;
    }

    public void setNutritionFacts(String nutritionFacts) {
        this.nutritionFacts = nutritionFacts;
    }

    public String getPrepSteps() {
        return this.prepSteps;
    }

    public void setPrepSteps(String prepSteps) {
        this.prepSteps = prepSteps;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getAuthorId() {
        return this.authorId;
    }

    public void setIngredients(Optional<Collection<RecipeIngredientInput>> ingredients) {
        this.ingredients = ingredients.orElse(Collections.emptyList());
    }

    public Collection<RecipeIngredientInput> getIngredients() {
        return this.ingredients;
    }

    public Recipe toModel() {
        Recipe recipe = new Recipe();
        recipe.setTitle(title); 
        recipe.setExcerpt(excerpt);
        recipe.setServings(servings);
        recipe.setPrepTime(prepTime);
        recipe.setNutritionFacts(nutritionFacts);
        recipe.setPrepSteps(prepSteps);
        return recipe;
    }

    public static class RecipeIngredientInput {
        private Long ingredientId;
        private Long quantity;

        public void setIngredientId(Long ingredientId) {
            this.ingredientId = ingredientId;
        }

        public Long getIngredientId() {
            return this.ingredientId;
        }

        public void setQuantity(Long quantity) {
            this.quantity = quantity;
        }

        public Long getQuantity() {
            return this.quantity;
        }
    }
}
