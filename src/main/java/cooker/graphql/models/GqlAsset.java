package cooker.graphql.models;

import cooker.models.Asset;

public class GqlAsset extends GqlBase<Asset> {
    public String name;
    public String type;
    public String url;
    public Long recipeId;

    public GqlAsset(Asset instance) {
        super(instance);
        this.name = instance.getName();
        this.type = instance.getType();
        this.url = instance.getUrl();
        this.recipeId = instance.getRecipeId();
    }
}
