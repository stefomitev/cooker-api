package cooker.graphql.models;

import cooker.models.Recipe;

public class GqlRecipe extends GqlBase<Recipe> {
    public String title;
    public String excerpt;
    public Integer servings;
    public Integer prepTime;
    public String nutritionFacts;
    public String prepSteps;
    public Long authorId;

    public GqlRecipe(Recipe instance) {
        super(instance);
        this.title = instance.getTitle();
        this.excerpt = instance.getExcerpt();
        this.servings = instance.getServings();
        this.prepTime = instance.getPrepTime();
        this.nutritionFacts = instance.getNutritionFacts();
        this.prepSteps = instance.getPrepSteps();
        this.authorId = instance.getAuthorId();
    }
}
