package cooker.graphql.loaders;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import com.google.common.collect.Lists;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import cooker.graphql.models.GqlAsset;
import cooker.graphql.models.GqlIngredient;
import cooker.graphql.models.GqlRecipe;
import cooker.graphql.models.GqlRecipeIngredient;
import cooker.graphql.models.GqlUser;
import cooker.repositories.AssetRepository;
import cooker.repositories.IngredientRepository;
import cooker.repositories.RecipeRepository;
import cooker.repositories.UserRepository;
import cooker.utils.ListUtils;

@Component
public class DataLoaderRegistryBuilder {
    public static String AUTHORS_FOR_RECIPES = "authorsForRecipes";
    public static String ASSETS_FOR_RECIPES = "assetsForRecipes";
    public static String INGREDIENTS_FOR_RECIPES = "ingredientsForRecipes";
    public static String INGREDIENTS = "ingredients";
    public static String RECIPES_FOR_AUTHORS = "recipesForAuthors";

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RecipeRepository recipeRepo;

    @Autowired
    private AssetRepository assetRepo;

    @Autowired
    private IngredientRepository ingredientRepo;

    public DataLoaderRegistry build() {
        DataLoaderRegistry registry = new DataLoaderRegistry();
        registry.register(AUTHORS_FOR_RECIPES, this.getAuthorsForRecipesLoader());
        registry.register(ASSETS_FOR_RECIPES, this.getAssetsForRecipesLoader());
        registry.register(INGREDIENTS_FOR_RECIPES, this.getIngredientsForRecipesLoader());
        registry.register(INGREDIENTS, this.getIngredientsLoader());
        registry.register(RECIPES_FOR_AUTHORS, this.getRecipesForAuthorsLoader());
        return registry;
    }

    private DataLoader<Long, GqlUser> getAuthorsForRecipesLoader() {
        return new DataLoader<Long, GqlUser>(
                recipeIds -> CompletableFuture.supplyAsync(() -> userRepo.getAuthorsForRecipes(recipeIds)
                        .parallelStream().map(user -> new GqlUser(user)).collect(Collectors.toList())));
    }

    private DataLoader<Long, List<GqlAsset>> getAssetsForRecipesLoader() {
        return new DataLoader<Long, List<GqlAsset>>(recipesIds -> CompletableFuture.supplyAsync(() -> {
            List<GqlAsset> allAssets = assetRepo.getAssetsForRecipes(recipesIds).parallelStream()
                    .map(asset -> new GqlAsset(asset)).collect(Collectors.toList());
            return ListUtils.<Long, GqlAsset>mapForKeys(recipesIds, allAssets, asset -> asset.recipeId);
        }));
    }

    private DataLoader<Long, List<GqlRecipeIngredient>> getIngredientsForRecipesLoader() {
        return new DataLoader<Long, List<GqlRecipeIngredient>>(recipesIds -> CompletableFuture.supplyAsync(() -> {
            List<GqlRecipeIngredient> allIngredients = ingredientRepo.getIngredientsForRecipes(recipesIds)
                    .parallelStream().map(recipeIngredient -> new GqlRecipeIngredient(recipeIngredient))
                    .collect(Collectors.toList());
            return ListUtils.<Long, GqlRecipeIngredient>mapForKeys(recipesIds, allIngredients,
                    recipeIngredient -> recipeIngredient.recipeId);
        }));
    }

    private DataLoader<Long, GqlIngredient> getIngredientsLoader() {
        return new DataLoader<Long, GqlIngredient>(ingredientIds -> CompletableFuture
                .supplyAsync(() -> Lists.newArrayList(ingredientRepo.findAllById(ingredientIds)).parallelStream()
                        .map(ingredient -> new GqlIngredient(ingredient)).collect(Collectors.toList())));
    }

    private DataLoader<Long, List<GqlRecipe>> getRecipesForAuthorsLoader() {
        return new DataLoader<Long, List<GqlRecipe>>(authorsIds -> CompletableFuture.supplyAsync(() -> {
            List<GqlRecipe> allRecipes = recipeRepo.getRecipesForAuthors(authorsIds).parallelStream()
                    .map(recipe -> new GqlRecipe(recipe)).collect(Collectors.toList());
            return ListUtils.<Long, GqlRecipe>mapForKeys(authorsIds, allRecipes, recipe -> recipe.authorId);
        }));
    }
}
