package cooker.models;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Recipe extends Base {
    @NotNull
    @Column(name = "title", unique = true)
    private String title;

    @NotNull
    @Column(name = "excerpt")
    private String excerpt;

    @NotNull
    @Column(name = "servings")
    private Integer servings;

    @NotNull
    @Column(name = "prep_time")
    private Integer prepTime;

    @NotNull
    @Column(name = "nutrition_facts")
    private String nutritionFacts;

    @NotNull
    @Column(name = "prep_steps")
    private String prepSteps;

    @OneToMany
    @JoinColumn(name = "recipe_id")
    private Collection<RecipeIngredient> ingredients;

    @OneToMany
    @JoinColumn(name = "recipe_id")
    private Collection<Asset> assets;

    @Column(name = "author_id", updatable = false, insertable = false)
    private Long authorId;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return this.excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public Integer getServings() {
        return this.servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public Integer getPrepTime() {
        return this.prepTime;
    }

    public void setPrepTime(Integer prepTime) {
        this.prepTime = prepTime;
    }

    public String getNutritionFacts() {
        return this.nutritionFacts;
    }

    public void setNutritionFacts(String nutritionFacts) {
        this.nutritionFacts = nutritionFacts;
    }

    public String getPrepSteps() {
        return this.prepSteps;
    }

    public void setPrepSteps(String prepSteps) {
        this.prepSteps = prepSteps;
    }

    public Iterable<RecipeIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Collection<RecipeIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Iterable<Asset> getAssets() {
        return assets;
    }

    public void setAssets(Collection<Asset> assets) {
        this.assets = assets;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getAuthorId() {
        return this.authorId;
    }
}
