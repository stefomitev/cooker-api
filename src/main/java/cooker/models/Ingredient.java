package cooker.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Ingredient extends Base {
    @NotNull
	@Column(name = "name", unique = true)
    private String name;

    @NotNull
	@Column(name = "base_unit")
    private String baseUnit;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUnit() {
        return this.baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }
}
