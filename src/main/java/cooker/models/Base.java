package cooker.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Base {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "base_gen")
	@SequenceGenerator(name = "base_gen", sequenceName = "base_seq", initialValue = 100, allocationSize = 5)
	protected Long id;

	@Column(name = "created_on")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createdOn;

	@Column(name = "modified_on")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date modifiedOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@PreUpdate
	private void preUpdate() {
		this.modifiedOn = new Date();
	}

	@PrePersist
	private void prePersist() {
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}
}
