INSERT INTO USER(ID, EMAIL, USERNAME, PASSWORD, CREATED_ON, MODIFIED_ON) VALUES
-- anonymous / None
(0, 'anonymous@system.com', 'anonymous', '----', NOW(), NOW()),
--- admin / admin
(1, 'admin@admin.com', 'admin', '$2a$04$IhGUBcjmDCcpZdC6hKnZuOjLW75uKBPJWP2oD13RT3BbhFXsC9Nsu', NOW(), NOW()),
-- stefo / enternow
(2, 'stefomitev@gmail.com', 'stefo', '$2a$04$MprORWV1XOzfgNere7/xMePh8PWLWln/HDb.TSbfOs70ZYEpLdJsq', NOW(), NOW());


INSERT INTO RECIPE(ID, TITLE, EXCERPT, SERVINGS, PREP_TIME, NUTRITION_FACTS, PREP_STEPS, AUTHOR_ID, CREATED_ON, MODIFIED_ON) VALUES
(3, 'Simple Recipe', 'Simple excerpt', 4, 3600, '250kCal', 'step1,step2,step3', 2, NOW(), NOW());

INSERT INTO RECIPE(ID, TITLE, EXCERPT, SERVINGS, PREP_TIME, NUTRITION_FACTS, PREP_STEPS, AUTHOR_ID, CREATED_ON, MODIFIED_ON) VALUES
(5, 'Simple Recipe 2', 'Simple excerpt', 4, 3600, '250kCal', 'step1,step2,step3', 2, NOW(), NOW());

INSERT INTO RECIPE(ID, TITLE, EXCERPT, SERVINGS, PREP_TIME, NUTRITION_FACTS, PREP_STEPS, AUTHOR_ID, CREATED_ON, MODIFIED_ON) VALUES
(6, 'Simple Recipe 3', 'Simple excerpt', 4, 3600, '250kCal', 'step1,step2,step3', 1, NOW(), NOW());

INSERT INTO RECIPE(ID, TITLE, EXCERPT, SERVINGS, PREP_TIME, NUTRITION_FACTS, PREP_STEPS, AUTHOR_ID, CREATED_ON, MODIFIED_ON) VALUES
(7, 'Simple Recipe 4', 'Simple excerpt', 4, 3600, '250kCal', 'step1,step2,step3', 1, NOW(), NOW());

INSERT INTO INGREDIENT(ID, NAME, BASE_UNIT, CREATED_ON, MODIFIED_ON) VALUES
(4, 'Milk', 'ml', NOW(), NOW());

INSERT INTO RECIPE_INGREDIENT(ID, INGREDIENT_ID, RECIPE_ID, QUANTITY, CREATED_ON, MODIFIED_ON) VALUES
(8, 4, 3, 250, NOW(), NOW());


-- INSERT INTO ROLE(ID, NAME, CREATED_ON, MODIFIED_ON) VALUES
-- (3, 'ROLE_USER', NOW(), NOW()),
-- (4, 'ROLE_ADMIN', NOW(), NOW());

-- INSERT INTO USERS_ROLES(USER_ID, ROLE_ID) VALUES
-- (1, 3),
-- (2, 3),
-- (2, 4);

-- INSERT INTO PRIVILEGE(ID, NAME, CREATED_ON, MODIFIED_ON) VALUES
-- (5, 'CREATE', NOW(), NOW()),
-- (6, 'READ', NOW(), NOW()),
-- (7, 'UPDATE', NOW(), NOW()),
-- (8, 'DELETE', NOW(), NOW());

-- INSERT INTO USERS_PRIVILEGES(USER_ID, PRIVILEGE_ID) VALUES
-- (0, 5);

-- INSERT INTO ROLES_PRIVILEGES(ROLE_ID, PRIVILEGE_ID) VALUES
-- (3, 5),
-- (3, 6),
-- (3, 7),
-- (4, 5),
-- (4, 6),
-- (4, 7),
-- (4, 8);

-- -- One document
-- INSERT INTO DOCUMENT(ID, NAME, TYPE, PATH, PARENT_ID, CREATED_ON, MODIFIED_ON) VALUES
-- (9, 'Document X', 'default', '/document-x', NULL, NOW(), NOW());

-- INSERT INTO WIDGET(ID, NAME, TYPE, PATH, PARENT_ID, CREATED_ON, MODIFIED_ON) VALUES
-- (10, 'Avatar Widget', 'avatar', '/document-x/avatar-widget', 9, NOW(), NOW());

-- INSERT INTO LAYOUT(ID, NAME, TYPE, PATH, PARENT_ID, CREATED_ON, MODIFIED_ON) VALUES
-- (11, 'Two Columns', 'two-column', '/document-x/two-columns', 9, NOW(), NOW());

-- INSERT INTO WIDGET(ID, NAME, TYPE, PATH, PARENT_ID, CREATED_ON, MODIFIED_ON) VALUES
-- (12, 'Experience', 'experience', '/document-x/two-columns/experience', 11, NOW(), NOW());

-- INSERT INTO WIDGET(ID, NAME, TYPE, PATH, PARENT_ID, CREATED_ON, MODIFIED_ON) VALUES
-- (13, 'Contact', 'contact', '/document-x/two-columns/contact', 11, NOW(), NOW());

-- INSERT INTO WIDGET(ID, NAME, TYPE, PATH, PARENT_ID, CREATED_ON, MODIFIED_ON) VALUES
-- (14, 'Paragraph', 'paragraph', '/document-x/two-columns/paragraph', 11, NOW(), NOW());

-- INSERT INTO PROPERTY(ID, CREATED_ON, MODIFIED_ON, NAME, VALUE) VALUES
-- (16, NOW(), NOW(), 'section', '1'),
-- (17, NOW(), NOW(), 'section', '2'),
-- (18, NOW(), NOW(), 'section', '2'),
-- (19, NOW(), NOW(), 'userProp', 'summary'),
-- (20, NOW(), NOW(), 'title', 'Profile'),
-- (21, NOW(), NOW(), 'documentId', '9');

-- INSERT INTO BASE_PROPERTY(BASE_ID, PROPERTY_ID) VALUES
-- (12, 16),
-- (13, 17),
-- (14, 18),
-- (14, 19),
-- (14, 20),
-- (1, 21);

-- -- Assets for components
-- INSERT INTO ASSET(ID, CREATED_ON, MODIFIED_ON, NAME, VALUE, TYPE, CATEGORY) VALUES
-- (22, NOW(), NOW(), 'avatar.js', 'http://localhost:3005/avatar/Avatar.js', 'SCRIPT', 'main'),
-- (23, NOW(), NOW(), 'avatar.hbs', 'http://localhost:3005/avatar/avatar.hbs', 'VIEW', 'view'),
-- (24, NOW(), NOW(), 'two-columns.hbs', 'http://localhost:3005/two-columns/two-columns.hbs', 'VIEW', 'view'),
-- (25, NOW(), NOW(), 'experience.js', 'http://localhost:3005/experience/Experience.js', 'SCRIPT', 'main'),
-- (26, NOW(), NOW(), 'experience.hbs', 'http://localhost:3005/experience/experience.hbs', 'VIEW', 'view'),
-- (27, NOW(), NOW(), 'contact.js', 'http://localhost:3005/contact/Contact.js', 'SCRIPT', 'main'),
-- (28, NOW(), NOW(), 'contact.hbs', 'http://localhost:3005/contact/contact.hbs', 'VIEW', 'view'),
-- (29, NOW(), NOW(), 'paragraph.js', 'http://localhost:3005/paragraph/Paragraph.js', 'SCRIPT', 'main'),
-- (30, NOW(), NOW(), 'paragraph.hbs', 'http://localhost:3005/paragraph/paragraph.hbs', 'VIEW', 'view'),
-- (31, NOW(), NOW(), 'source-sans-pro', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro', 'STYLE', 'font');
-- --(33, NOW(), NOW(), 'avatar.module.css', 'http://localhost:3005/avatar/avatar.module.css', 'STYLE', 'style');

-- INSERT INTO ITEMS_ASSETS(ITEM_ID, ASSET_ID) VALUES
-- (10, 22),
-- (10, 23),
-- (11, 24),
-- (12, 25),
-- (12, 26),
-- (13, 27),
-- (13, 28),
-- (14, 29),
-- (14, 30),
-- (9, 31);
-- --(10, 33);
